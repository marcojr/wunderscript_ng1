angular
    .module('app')
    .factory('Solutions',['$http', '$rootScope', function($http, $rootScope) {
        return {
            listSolutions: function (clientId, sessionId) {
                var call = $rootScope.backendServer + 'solutions/list/' + clientId +'/' + sessionId;
                return $http.get(call);
            },
            allocateSeats: function (clientId, sessionId, apps) {
                var call = $rootScope.backendServer + 'seats/allocate/' + clientId +'/' + sessionId;
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                };
                var data = $.param(apps);
                return $http.post(call, data, config);
            },
            deleteSolution: function (clientId, sessionId, appId) {
                var call = $rootScope.backendServer + 'solutions/delete/' + clientId +'/' + sessionId +'/' + appId;
                return $http.get(call);
            },
            payForUpgrade: function (clientId, sessionId, appId, months) {
                var call = $rootScope.backendServer + 'solutions/upgrade/' + clientId +'/' + sessionId +'/' + appId +'/' + months;
                return $http.get(call);
            },
            buySeats: function (clientId, sessionId, qty) {
                var call = $rootScope.backendServer + 'seats/buy/' + clientId +'/' + sessionId +'/' + qty;
                return $http.get(call);
            }
        }
    }]);