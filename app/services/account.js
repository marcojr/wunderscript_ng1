angular
    .module('app')
    .factory('Account',['$http', '$rootScope', function($http, $rootScope) {
        return {
            login: function (email, password) {
                var call = $rootScope.backendServer + 'account/login';
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                };
                var data = $.param({
                    email: email,
                    password: password
                });
                return $http.post(call, data, config);
            },
            changeInfo: function (clientId, sessionId, newInfo) {
                var call = $rootScope.backendServer + 'account/changeInfo/' + clientId +'/' + sessionId;
                var config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                };
                var data = $.param(newInfo);
                return $http.post(call, data, config);
            },
            confirmPhone: function (clientId, sessionId, code) {
                var call = $rootScope.backendServer + 'account/confirmPhone/' + clientId +'/' + sessionId +'/' + code;
                return $http.get(call);
            },
            validateSession: function (sessionId) {
                var call = $rootScope.backendServer + 'account/session/validate/' +  sessionId;
                return $http.get(call);
            },
            logoff: function (clientId, sessionId) {
                var call = $rootScope.backendServer + 'account/session/kill/' +  clientId +'/' + sessionId;
                return $http.get(call);
            }
        }
    }]);