angular
    .module('app')
    .controller('ModalInstanceConfirmPhoneCtrl', function ($scope,$uibModalInstance) {
        $scope.code = "";
        $scope.invalidCode = false;
        $scope.rxCode = /^\+[1-9]{1}[0-9]{3,14}$/;
        $scope.ok = function () {
            $uibModalInstance.close($scope.code);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });