angular
    .module('app')
    .controller('ModalInstanceNewSolutionCtrl', function ($scope,$rootScope, $uibModalInstance, FileUploader) {
        $scope.uploader = new FileUploader({
            url : $rootScope.backendServer +'upload/package/' + $rootScope.session.clientId +'/' + $rootScope.session.sessionId
        });
        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers){
            $uibModalInstance.close(response);
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })