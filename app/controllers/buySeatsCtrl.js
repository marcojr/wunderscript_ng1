angular
    .module('app')
    .controller('ModalInstanceBuySeatsCtrl', function ($scope,$rootScope, $uibModalInstance) {
        $scope.qty = [];
        for(var i=1 ; i < 21; i++){
            $scope.qty.push({
                seats: i,
                price : 'Qty : ' + i + ' = ' + $rootScope.session.pricePolicies.currencySymbol + ' ' + (($rootScope.session.pricePolicies.seats[0].price) * i).toFixed(2) +' + vat'
            });
        }
        $scope.chooseQty = $scope.qty[0];
        $scope.ok = function () {
            $uibModalInstance.close($scope.chooseQty.seats);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });