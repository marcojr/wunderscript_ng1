angular
    .module('app')
    .controller('ModalInstanceUpgradeSolutionCtrl', function ($scope,$rootScope, $uibModalInstance, appId) {
        $scope.prices = $rootScope.session.pricePolicies.solutions;
        $scope.currencySymbol = $rootScope.session.pricePolicies.currencySymbol;
        $scope.indexSelected = 1;
        $scope.ok = function () {
            $uibModalInstance.close({
                appId: appId,
                months: $scope.indexSelected
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });