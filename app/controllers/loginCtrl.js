angular
    .module('app')
    .controller('loginCtrl',['$scope','$rootScope','$location','$cookies','Account', function($scope,$rootScope,$location,$cookies, Account) {
        $scope.email = '';
        $scope.password = '';
        $scope.invalidLogin = false;
        if(typeof($cookies.get('sessionId')) != 'undefined'){
            Account.validateSession($cookies.get('sessionId'))
                .then(function(response) {
                    var data = response.data;
                    if(!data.valid) {
                        return;
                    }
                    $rootScope.session = {
                        sessionId: $cookies.get('sessionId'),
                        name: data.name,
                        clientId: data.clientId,
                        creditsAvailable: data.creditsAvailable,
                        contactName: data.contactName,
                        contactEmail: data.contactEmail,
                        mobilePhone: data.mobilePhone,
                        email: data.email,
                        status: data.status,
                        pricePolicies: data.pricePolicies
                    };
                    $location.path('/cms');
                });
        }
        $scope.doLogin = function(){
            var emailRegEx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
            if(!emailRegEx.test($scope.email)) { return; }
            Account.login($scope.email, $scope.password)
                .then(function(response) {
                    var data = response.data;
                    if(data.authorized){
                        $rootScope.session = {
                            sessionId : data.sessionId,
                            name: data.name,
                            clientId: data.clientId,
                            creditsAvailable: data.creditsAvailable,
                            contactName: data.contactName,
                            contactEmail: data.contactEmail,
                            mobilePhone: data.mobilePhone,
                            email: data.email,
                            status: data.status,
                            pricePolicies: data.pricePolicies
                        };
                        var expireDate = new Date();
                        expireDate.setDate(expireDate.getDate() + 15);
                        $cookies.put('sessionId',data.sessionId, {expires: expireDate});
                        $location.path('/cms');
                    }
                    else
                    {
                        $scope.invalidLogin = true;
                        $scope.email = '';
                        $scope.password = '';
                    }
                });
        }
    }]);