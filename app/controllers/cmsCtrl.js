angular
    .module('app')
    .controller('cmsCtrl',['$scope','$rootScope','$location','$uibModal','$window','$cookies','Solutions', 'Account', function($scope,$rootScope,$location,$uibModal,$window, $cookies, Solutions, Account) {
        setModals();
        $scope.busy = true;
        if(typeof($rootScope.session) == 'undefined') {
            if(typeof($cookies.get('sessionId')) == 'undefined'){
                $scope.busy = false;
                $location.path('/login');
                return;
            }
            else
            {
                Account.validateSession($cookies.get('sessionId'))
                    .then(function(response) {
                        var data = response.data;
                        if(!data.valid) {
                            $scope.busy = false;
                            $location.path('/login');
                            return;
                        }
                        $rootScope.session = {
                            sessionId: $cookies.get('sessionId'),
                            name: data.name,
                            clientId: data.clientId,
                            creditsAvailable: data.creditsAvailable,
                            contactName: data.contactName,
                            contactEmail: data.contactEmail,
                            mobilePhone: data.mobilePhone,
                            email: data.email,
                            status: data.status,
                            pricePolicies: data.pricePolicies
                        };
                        readSession();
                    });
            }
        }
        else {
            readSession();
        }
        function readSession(){
            $scope.backendServer = $rootScope.backendServer;
            $scope.clientId = $rootScope.session.clientId;
            $scope.sessionId = $rootScope.session.sessionId;
            $scope.availableSeats = $rootScope.session.creditsAvailable;
            $scope.AvailableSeats = $rootScope.session.creditsAvailable;
            $scope.availableSeatsSession = $rootScope.session.creditsAvailable;
            $scope.companyName = $rootScope.session.name;
            loadSolutions();
        }
        function loadSolutions() {
            Solutions.listSolutions($rootScope.session.clientId, $rootScope.session.sessionId)
                .then(function(response) {
                    if(!response.data.successfully){
                        $scope.logoff();
                        return;
                    }
                    $scope.solutions = response.data.apps;
                    formatDataSet();
                    $scope.busy = false;
                });
        }
        $scope.incDecSeat = function(ndx, incOrDec){
            if(incOrDec=='i' && $scope.availableSeats >0){
                $scope.availableSeats--;
                $scope.solutions[ndx].addIncSeat++;
            }
            if(incOrDec=='d'){
                if($scope.solutions[ndx].addIncSeat > 0) {
                    $scope.availableSeats++;
                    $scope.solutions[ndx].addIncSeat--;
                }
            }
        }
        $scope.allocateSeats = function() {
            if($scope.oriAvailableSeats == $scope.availableSeats) { return;}
            var data = { apps : []};

            for(var i =0 ; i < $scope.solutions.length; i++){
                data.apps.push({
                    appId: $scope.solutions[i].appId,
                    addedSeats: $scope.solutions[i].addIncSeat
                });
            }
            Solutions.allocateSeats($rootScope.session.clientId, $rootScope.session.sessionId,data)
                .then(function(response) {
                    if(response.data.successfully){
                        $rootScope.session.creditsAvailable = $scope.availableSeats;
                        $scope.oriAvailableSeats = $scope.availableSeats;
                        loadSolutions();
                    }
                });
        }
        $scope.deleteSolution = function(appId, name){
            if(prompt("This will delete the whole solution including and all the seats assigned to " + name +" Also, all the assets and the scripts will be lost. Type YES bellow to confirm.").toLowerCase() == 'yes'){
                Solutions.deleteSolution($rootScope.session.clientId, $rootScope.session.sessionId,appId)
                    .then(function(response) {
                        loadSolutions();
                    });
            }
        }
        $scope.logoff = function (){
            $cookies.remove('sessionId');
            Account.logoff($rootScope.session.clientId,$rootScope.session.sessionId)
                .then(function () {
                    $location.path('/login');
                });
        }
        function formatDataSet(){
            for(var i=0; i < $scope.solutions.length;i++){
                $scope.solutions[i].addIncSeat = 0;
                $scope.solutions[i].expires = formatDate($scope.solutions[i].expires);
            }
        }
        function formatDate(isoDate) {
            var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var d = new Date(isoDate);
            return shortMonths[d.getMonth()] +' ' + pad(d.getDate(), 2) +  ", " + d.getFullYear();
        }
        function pad(num,size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }
        function setModals() {
            $scope.openChangeDetails = function () {
                var modalInstanceChangeDetails = $uibModal.open({
                    animation: true,
                    templateUrl: '/app/modal/changeAccount.html',
                    controller: 'ModalInstanceChangeDetailsCtrl'
                });
                modalInstanceChangeDetails.result.then(function (data) {
                    Account.changeInfo($scope.session.clientId, $scope.session.sessionId, data)
                        .then(function(response) {
                            if(response.data.successfully){
                                $rootScope.session.name = data.companyName;
                                $rootScope.session.contactName = data.contactName;
                                $rootScope.session.email = data.email;
                                $scope.companyName = data.companyName;
                                if(response.data.phoneCodeSent){
                                    $scope.openConfirmPhone();
                                }
                            }
                        });
                });
            };
            $scope.openConfirmPhone = function () {
                var modalInstanceConfirmCode = $uibModal.open({
                    animation: true,
                    templateUrl: '/app/modal/confirmPhone.html',
                    controller: 'ModalInstanceConfirmPhoneCtrl'
                });
                modalInstanceConfirmCode.result.then(function (data) {
                    Account.confirmPhone($scope.session.clientId, $scope.session.sessionId, data)
                        .then(function(response) {
                            if(response.data.confirmed){
                                $rootScope.session.mobilePhone = response.data.phoneNumber;
                            }
                            else
                            {
                                setTimeout(function(){ $scope.openConfirmPhone(); }, 3000);
                            }
                        });
                });
            };
            $scope.openNewSolution = function () {
                var modalInstanceNewSolution = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: '/app/modal/newSolution.html',
                    controller: 'ModalInstanceNewSolutionCtrl'
                });
                modalInstanceNewSolution.result.then(function (data) {
                    if(data.successfully) {
                        setTimeout(function () {loadSolutions();}, 2000);
                    }
                });
            };
            $scope.openUpdateSolution = function (appId) {
                var modalInstanceUpdateSolution = $uibModal.open({
                    animation: true,
                    templateUrl: '/app/modal/updateSolution.html',
                    controller: 'ModalInstanceUpdateSolutionCtrl',
                    resolve: {appId: function () {return appId;}}
                });
                modalInstanceUpdateSolution.result.then(function (data) {
                    if(data.successfully) {
                        setTimeout(function () {loadSolutions();}, 2000);
                    }
                });
            };
            $scope.openUpgradeSolution = function (appId) {
                var modalInstanceUpgradeSolution = $uibModal.open({
                    animation: true,
                    templateUrl: '/app/modal/upgradeSolution.html',
                    controller: 'ModalInstanceUpgradeSolutionCtrl',
                    resolve: {appId: function () {return appId;}}
                });
                modalInstanceUpgradeSolution.result.then(function (data) {
                    Solutions.payForUpgrade($scope.session.clientId, $scope.session.sessionId, data.appId, data.months)
                        .then(function(response) {
                            if(response.data.successfully){
                                $window.location.href = response.data.url;
                            }
                        });
                });
            };
            $scope.openBuySeats = function (appId) {
                var modalInstanceBuySeats = $uibModal.open({
                    animation: true,
                    templateUrl: '/app/modal/buySeats.html',
                    controller: 'ModalInstanceBuySeatsCtrl',
                    resolve: {appId: function () {return appId;}}
                });
                modalInstanceBuySeats.result.then(function (data) {
                    Solutions.buySeats($scope.session.clientId, $scope.session.sessionId, data)
                        .then(function(response) {
                            if(response.data.successfully){
                                $window.location.href = response.data.url;
                            }
                        });
                });
            };
        }
    }])

