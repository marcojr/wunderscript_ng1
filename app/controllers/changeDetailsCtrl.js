angular
    .module('app')
    .controller('ModalInstanceChangeDetailsCtrl', function ($scope, $rootScope,  $uibModalInstance) {
        $scope.cName = $rootScope.session.name;
        $scope.contact = $rootScope.session.contactName;
        $scope.email = $rootScope.session.email;
        $scope.mobile = $rootScope.session.mobilePhone;
        $scope.pass1 = "";
        $scope.pass2 = "";
        $scope.rxMobile = /^\+[1-9]{1}[0-9]{3,14}$/;
        $scope.rxPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
        //$scope.rxMobile= "^\+\d{10}\b";
        $scope.ok = function () {
            if(($scope.pass1 != "" || $scope.pass2 !="") && $scope.pass1 != $scope.pass2){
                return;
            }
            if($scope.form.$valid){
                $uibModalInstance.close(
                    {
                        companyName : $scope.cName,
                        contactName : $scope.contact,
                        email: $scope.email,
                        mobileNumber: $scope.mobile,
                        password: $scope.pass1
                    });
            }
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });