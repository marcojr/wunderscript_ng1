angular
    .module('app')
    .controller('ModalInstanceUpdateSolutionCtrl', function ($scope,$rootScope, $uibModalInstance, FileUploader, appId) {
        $scope.uploader = new FileUploader({
            url : $rootScope.backendServer +'update/package/' + $rootScope.session.clientId +'/' + $rootScope.session.sessionId +'/' + appId
        });
        $scope.uploader.onSuccessItem = function(fileItem, response, status, headers){
            $uibModalInstance.close(response);
        }
        $scope.ok = function () {
            //$uibModalInstance.close($scope.code);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });