angular
    .module('app',[
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'angularFileUpload',
        'ngCookies'
    ])
    .run(function($rootScope) {
        $rootScope.backendServer = "http://localhost:5000/";
        //$rootScope.backendServer = "http://www.wunderscript.com/";
        $rootScope.prices = {
            currency: 'GBP',
            currencySymbol : '£',
            seats : [
                {
                quantity: 1,
                    price: 14.99,
                    vat: 20
                }
            ],
            solutions : [
                {
                    months: 12,
                    label : "1 year",
                    price: 1042.89,
                    vat: 20
                }
            ]
        }
    })
    .config(['$urlRouterProvider','$stateProvider', function($urlRouterProvider,$stateProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('login',{
                url: '/login',
                templateUrl: './app/templates/login.html',
                controller: 'loginCtrl'
            })
            .state('cms',{
                url: '/cms',
                templateUrl: './app/templates/cms.html',
                controller : 'cmsCtrl'
            })
    }])
    .controller(function ($scope){

    })