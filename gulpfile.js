var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var path = require("path");
var url = require("url");
var argv = require('yargs').argv;
var runSequence = require('run-sequence');
gulp.task('default', ['run']);
gulp.task('sass', function() {
    console.log('compiling sass...');
    return gulp.src('./style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('.'))
        .pipe(browserSync.stream());
});
gulp.task('run', ['sass'], function() {
    browserSync.init({
        server: {
            baseDir: "./",
        }
    });
    gulp.watch(["./style.scss"], ['sass']);
    gulp.watch(["index.html" , "./app/**/*.html", "app/**/*.js"]).on('change', browserSync.reload);
});